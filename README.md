# Projet tuteuré 

 Dans le cadre de notre étude comparative entre les plantes aquatiques, terrestres et les
 algues, notre client utilise le logiciel BRAKER (Hoff KJ et al., 2016) pour prédire la position et
 la structure des gènes des génomes étudiés.  
Cependant, malgré sa renommée, BRAKER présente des limitations, notamment des erreurs possibles dans la prédiction de la structure des gènes.  
 Notre client a relevé plusieurs anomalies telles que des extrémités manquantes, des introns plus longs ou supplémentaires, des fusions de gènes, l'identification d'un exon
 fonctionnel en intron, et des erreurs de prédiction des sites d'épissage.  
  Ces imprécisions peuvent entraîner une confusion entre un pseudogène et un gène fonctionnel. 

 Pour pallier ces anomalies, notre équipe a entrepris une correction de ces erreurs 
 d’annotation afin de ré-annoter les génomes végétaux et de comparer des gènes
 fonctionnels ou potentiels pseudogènes dans divers groupes de Viridiplantae et Plantae.  
  Plus spécifiquement, nous visons à comparer les erreurs d’annotation récurrentes, qui sont des
 potentiels pseudogènes, au sein de divers groupes de Viridiplantae et Plantae, afin
 d'explorer leur rôle dans l'adaptation des plantes à des environnements aquatiques ou
 terrestres.
 ## Objectifs:
_Annoter les génomes de plantes vertes avec BRAKER et corriger les erreurs d'annotations.  
_Réaliser un test d’enrichissement des fonctions biologiques sur les erreurs
 d'annotation.  
_Comparer les événements de perte de gènes entre les différentes espèces  
 ## Matériels & Méthodes:  
 Pour effectuer ce travail de recherche, nous avions accès au [cluster de calculs intensifs de Genotoul](https://bioinfo.genotoul.fr/index.php/genologin-to-genobioinfo/).

Consultez le rapport complet pour explorer les données, les techniques utilisées et les résultats obtenus.  
[Rapport complet](https://gitlab.com/oumarklf/ptut/-/blob/main/Rapport/Rapport_projet_CoBRA.pdf)

