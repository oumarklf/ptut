# -*- coding: utf-8 -*-
"""
Created on Sun Mar 24 16:32:37 2024

@author: Mbow
"""
import sys
def extraire_sequences_introns(fichier_intron, fichier_ara):
    sequences_introns = {}

    # Lecture des noms d'introns à partir du fichier intron
    with open(fichier_intron, 'r') as f_intron:
        noms_introns = [ligne.strip() for ligne in f_intron.readlines()]

    # Parcours du fichier ara.txt pour chercher les séquences introniques correspondantes
    with open(fichier_ara, 'r') as f_ara, open(sys.argv[3], 'w') as f_resultat:
        lignes = f_ara.readlines()
        nom_intron = None
        sequence_intron = None
        for ligne in lignes:
            if ligne.startswith(">"):  # Ligne contenant un nom d'intron
                if nom_intron in noms_introns:  # Si nous avons déjà trouvé une correspondance
                    f_resultat.write(f"{nom_intron}\n{sequence_intron}\n\n")  # Écrire dans le fichier
                nom_intron = ligne.strip()  # Mise à jour du nom de l'intron en cours de traitement
                sequence_intron = ""  # Réinitialisation de la séquence intronique
            else:  # Ligne contenant une séquence intronique
                sequence_intron += ligne.strip().upper()  # Ajout de la séquence intronique

        # Ajout de la dernière séquence intronique si elle correspond à un nom d'intron
        if nom_intron in noms_introns:
            f_resultat.write(f"{nom_intron}\n{sequence_intron}\n\n")  # Écrire dans le fichier
            
# Utilisation de la fonction avec les fichiers fournis
fichier_intron =sys.argv[1]  # Remplacer par le chemin réel de votre fichier d'introns
fichier_ara = sys.argv[2]  # Remplacer par le chemin réel de votre fichier ara.txt
fichier_resultat = sys.argv[3]  # Remplacer par le chemin réel de votre fichier de résultat
extraire_sequences_introns(fichier_intron, fichier_ara)

print(f"Les séquences introniques ont été écrites dans le fichier {fichier_resultat}")

