#!/bin/bash
#SBATCH -p workq
#SBATCH -c 12
#SBATCH --cpus-per-task=12
#SBATCH --mem-per-cpu=480

# Load modules
module load bioinfo/NCBI_Blast+/2.15.0+
module load bioinfo/EMBOSS/6.6.0
module load devel/java/17.0.6 devel/python/Python-3.6.3
module load bioinfo/InterProScan/5.51-85.0

# Display help if the -h option is specified
if [[ "$1" == "-h" || "$1" == "--help" ]]; then
    echo "This program corrects a BRAKER gene prediction by searching for potential pseudogenes on the introns. Usage is:"
    echo ""
    echo "Usage: $0 <gff_file> <fasta_file> <min_identity> <min_length>"
    echo "    gff_file: BRAKER output file in GFF3 format"
    echo "    fasta_file: file containing the entire sequence in FASTA format"
    echo "    min_identity: minimum percentage identity to filter blast result"
    echo "    min_length: minimum length to filter blast result"
    exit 0
fi

# Extract input file names
braker_gff="$1"
sequence_fna="$2"

# Check if input files are specified
if [ -z "$braker_gff" ] || [ -z "$sequence_fna" ]; then
    echo "Please specify braker.gff3 and sequence.fna files as input."
    exit 1
fi

# Run the Extract intron script
python3 Extract_intron.py "$braker_gff" "$sequence_fna" introns.txt

# Find ORFs on the retrieved intron file
getorf -sequence introns.txt -outseq resultatorf.fasta -minsize 300 -find 0

# Perform a blast on the ORFs
blastp -query resultatorf.fasta -db uniprot_sprot -num_alignments 1 -outfmt 7 -out proteins_blastp_orf.txt

# Filter the blast using user-specified values
awk -v min_identity="$3" -v min_length="$4" '/^# Query:/ {query_line=$0} $3+0>=min_identity && $4+0>min_length && query_line {print query_line}' proteins_blastp_orf.txt > resultatawk.txt
awk '{sub(/^# Query: /, ">")}1' resultatawk.txt > resultatawk2.txt

# Retrieve corresponding sequences
python3 Extract_Intron2.2.py resultatawk2.txt resultatorf.fasta orf_filtre.txt

# Run an InterProScan search
interproscan.sh -mode cluster -clusterrunid Test_cluster_$SLURM_JOBID -i orf_filtre.txt -b output_cluster_$SLURM_JOBID -dp --goterms

# Retrieve Go terms for enrichment test on Rstudio
grep 'Ontology_term' output_cluster_$SLURM_JOBID.gff3 > out.txt
awk '{print $1, $10, $11}' out.txt > out2.txt
grep -o 'Ontology_term="[A-Z0-9:]*"' out2.txt | cut -d'"' -f2 | sort | uniq -c | sort -rn > Go_terms.txt
rm out.txt
rm out2.txt
# Search for proteins corresponding to SwissProt identifiers
awk -v min_identity="$3" -v min_length="$4" '$3+0 >= min_identity && $4+0>=min_length {split($2, id, "|"); print id[1] "|" id[2]}' proteins_blastp_orf.txt > id_swissprot.txt

# Search for information in the SwissProt database
while IFS= read -r line; do
    grep "$line" /bank/ebi/uniprot/current/fasta/uniprot_sprot.fasta >> proteines_orf.txt
done < id_swissprot.txt
#modify the gff files
python3 correction_gff.py "$braker_gff" resultatawk2.txt braker_tmp.gff3

sort --version-sort -k1,1 -k4,4 braker_tmp.gff3 > corrected_braker.gff3
rm braker_tmp.gff3