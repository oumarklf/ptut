# -*- coding: utf-8 -*-
import sys

exons = {}
sequences = {}
seq_name = {}

def remove_introns(gff_file, output_file):
    with open(gff_file, 'r') as f:
        lines = f.readlines()

    with open(output_file, 'w') as f:
        for line in lines:
            if '\tintron\t' not in line:  # V�rifie si la ligne ne contient pas 'intron'
                f.write(line)
                
def exon_prep(exon_file):
    
    with open(exon_file, 'r') as f:
        for line in f:
          elements = line.strip().split()
          
          sequence_id_full = elements[0]
          sequence_id = sequence_id_full.split("_")[1]
          sequence_id = ""+sequence_id 
          
          
          start_position = int(elements[1].strip("["))
          end_position = int(elements[3].strip("]"))
          if (start_position > end_position):
              start_position,end_position=end_position,start_position
          
          if sequence_id in exons and [start_position, end_position] not in exons[sequence_id]:
              exons[sequence_id].append([start_position, end_position])
          else:
              exons[sequence_id] = [[start_position, end_position]]
    #print(exons)
    
def gff_correction(output_file):
    
    with open(output_file, "r") as f:
      for line in f:
            # S�paration de chaque ligne en �l�ments
          elements = line.strip().split("\t")

        # R�cup�ration des informations pertinentes
          sequence_id_full = elements[0]
          #print(sequence_id_full)
          sequence_id = sequence_id_full.split("_")[1]  # Extraction de la partie apr�s le premier underscore
          if sequence_id not in sequences:
              sequences[sequence_id] = {}
              seq_name[sequence_id] = sequence_id_full

          feature_type = elements[2]
          start_position = int(elements[3])

        # Si le type d'entit� est un g�ne, r�cup�rer le d�but
          if feature_type == "gene":
               sequences[sequence_id] = start_position

    #print(seq_name)
    
    # Ecriture des nouveaux exon dans le fichier
    with open(output_file, 'a') as f:
            for exon_id in exons:
                for pos in exons[exon_id]:
                    f.write(f'{seq_name[exon_id]}\tAUGUSTUS\tcorrected_exon\t{pos[0] + sequences[exon_id]}\t{pos[1] + sequences[exon_id]}\t.\t.\t.\t Exon corrected from Braker annotation\n')


# Utilisation du programme
# python3 correction_gff.py gff_file exon_file outfile

gff_file = sys.argv[1]
exon_file = sys.argv[2]
output_file = sys.argv[3]

remove_introns(gff_file, output_file)
exon_prep(exon_file)
gff_correction(output_file)

